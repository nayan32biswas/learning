import numpy
from time import time
import code_cython
import code_python

N = 1000000
py_time, cy_time = 0, 0
data_list = numpy.random.randint(low=1, high=N*N, size=N)


def test_python():
    global py_time
    start = time()
    result = code_python.total_tax(data_list, N)
    py_time += (time()-start)


def test_cython():
    global cy_time
    start = time()
    result = code_cython.total_tax(data_list, N)
    cy_time += (time()-start)


if __name__ == "__main__":
    for i in range(50000):
        if i % 2 == 0:
            test_python()
            test_cython()
        else:
            test_cython()
            test_python()

    print(f"py: {py_time*1000}")
    print(f"cy: {cy_time*1000}")
