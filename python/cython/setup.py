from distutils.core import setup
from Cython.Build import cythonize

# setup(ext_modules=cythonize('code_cython.pyx'))
setup(ext_modules=cythonize('code_cython_mult_core.pyx'))
