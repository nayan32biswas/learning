import datetime
import multiprocessing
import concurrent.futures
from PIL import Image
from pathlib import Path


def task_convert_image(file_name, size=(1500, 1500), profile_avatar=False):
    try:
        image = Image.open(f"image/{file_name}")
        image.thumbnail(size, Image.ANTIALIAS)
        prefix = datetime.datetime.utcnow().strftime('%H%M%S%f')
        if profile_avatar:
            image.save(f"image/new/ava-{prefix}-{file_name}", quality=75)
        else:
            image.save(f"image/new/pro-{prefix}-{file_name}", quality=75)
    except Exception as error:
        print(str(error))
        return 1
    return 0


def convert_profile(profile_image=None, profile_avatar=None): # Slowes one regular method
    task_convert_image(profile_image, size=(700, 700), profile_avatar=False)
    task_convert_image(profile_image, size=(100, 100), profile_avatar=True)


# def convert_profile_multi(profile_image=None, profile_avatar=None): # midilest one by CPU bound
#     p_ima = multiprocessing.Process(target=task_convert_image, args=[profile_image, (700, 700), False])
#     p_ava = multiprocessing.Process(target=task_convert_image, args=[profile_image, (100, 100), True])

#     p_ima.start(), p_ava.start()
#     p_ima.join(), p_ava.join()


# def convert_profile_io(profile_image=None, profile_avatar=None): # fastest one by IO bound
#     with concurrent.futures.ThreadPoolExecutor() as executor:
#         results = []
#         results.append(executor.submit(task_convert_image, profile_image, (700, 700), False))
#         results.append(executor.submit(task_convert_image, profile_image, (100, 100), True))
#         results = [f.result() for f in concurrent.futures.as_completed(results)]
