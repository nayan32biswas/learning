import datetime
from PIL import Image
from pathlib import Path


cpdef task_convert_image(str file_name, int[:] size=(1500, 1500), bool profile_avatar=False):
    try:
        image = Image.open(f"image/{file_name}")
        image.thumbnail(size, Image.ANTIALIAS)
        prefix = datetime.datetime.utcnow().strftime('%H%M%S%f')
        if profile_avatar:
            image.save(f"image/new/ava-{prefix}-{file_name}", quality=75)
        else:
            image.save(f"image/new/pro-{prefix}-{file_name}", quality=75)
    except Exception as error:
        print(str(error))
        return 1
    return 0


cpdef convert_profile(str profile_image=None, str profile_avatar=None):
    task_convert_image(profile_image, size=(700, 700), profile_avatar=False)
    task_convert_image(profile_image, size=(100, 100), profile_avatar=True)
