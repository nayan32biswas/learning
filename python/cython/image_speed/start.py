import os
import time
import multiprocessing

from image_file import (
    convert_profile
)


def timeit(method):
    def timed(*args, **kwargs):
        start = time.time()
        result = method(*args, **kwargs)
        print(f"{method.__name__}-->  {(time.time()-start)*1000:,.3f} ms")
        return result
    return timed


def get_images():
    path = "/home/nayan32biswas/prog/learning/language/python/cython/image_speed/image"
    valid_images = [".jpg", ".gif", ".png", ".tga"]
    imgs = [f for f in os.listdir(path) if os.path.splitext(f)[1].lower() in valid_images]
    return imgs


@timeit
def convert_image_py(image_list):
    for image in image_list:
        convert_profile(profile_image=image, profile_avatar=image)



if __name__ == "__main__":
    image_list = get_images()
    convert_image_py(image_list)


# def multi_proc(it):
#     print(f"start {it}")
#     time.sleep(1)
#     print(f"end {it}")
# pid = [1, 2, 3, 4]
# process_list = [multiprocessing.Process(target=multi_proc, args=[it]) for it in pid]
# temp = [p.start() for p in process_list]
# temp2 = [p.join() for p in process_list]