import numpy
from time import time

import code_cython
import code_python

N = 10000000
cy_time, cy_time_mult = 0, 0
data_list = numpy.random.randint(low=1, high=N*N, size=N)


def test_cython():
    global cy_time
    start = time()
    result = code_cython.total_tax(data_list, N)
    cy_time += (time()-start)


def test_cython_mult_core():
    import code_cython_mult_core
    global cy_time_mult
    start = time()
    

    from concurrent.futures import ThreadPoolExecutor
    with ThreadPoolExecutor(max_workers=4) as executor:
        sections = numpy.array_split(data_list, 4)
        jobs = [executor.submit(code_cython_mult_core.total_tax_cal, s) for s in sections]
    result = sum(job.result() for job in jobs)

    cy_time_mult += (time()-start)


if __name__ == "__main__":
    for i in range(50):
        if i % 2 == 0:
            test_cython()
            test_cython_mult_core()
        else:
            test_cython_mult_core()
            test_cython()

    print(f"cy: {cy_time*1000}")
    print(f"mult: {cy_time_mult*1000}")
