import csv
import requests, bs4

f = open("data.csv", "w", newline="")
write = csv.writer(f)

req = requests.get("https://en.wikipedia.org/wiki/Corruption_Perceptions_Index")
soup = bs4.BeautifulSoup(req.text, "lxml")
table = soup("table")[4].find_all("tr")
for row in table:
    cols = row.findChildren(recursive=False)
    cols = [ele.text.strip() for ele in cols]
    write.writerow([ele.encode("utf-8") for ele in cols])