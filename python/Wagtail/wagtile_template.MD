**File naming convention**
```html
<<app_name>>/<<model_name>>.html
Here model_name should be versone of snake_case.
```

**Start with this line**
```html
{% load wagtailimages_tags %}
```

**Get data**
```html
For Details
`{{ self.data }}`
For List
{% for data in page %}
    {{data.data}}
{% endfor %}
```

**For richtext**
```html
{{ page.body|richtext }}
```


**Image**
```html
{% image [image] [resize-rule] %}

{% image page.photo original %}
{% image page.photo width-400 %}
{% image page.photo fill-80x80 %}
{% image page.photo min-500x200 %}
{% image page.photo height-480 %}
{% image page.photo scale-50 %}
{% image page.photo fill-200x200 %}
{% image page.photo fill-200x200-c100 %}
```
