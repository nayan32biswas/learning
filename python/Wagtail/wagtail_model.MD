**Wagtail Import**
```python
from modelcluster.fields import ParentalKey
from wagtail.core.models import (
    Page, Orderable, PageManager
)
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import (
    FieldPanel, StreamFieldPanel, MultiFieldPanel, InlinePanel
)
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.search import index
```

**Example**
```python
# Here stream is an app and block is a file.
from stream import block
class HomePage(Page):
    body = RichTextField()
    date = models.DateField("Post date")
    feed_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True, blank=True,
        on_delete=models.SET_NULL, related_name='+'
    )
    content = StreamField(
        [
            (“Title_and_text”, blocks.TitleAndTextBlock())
        ],
        null=True, blank=True,
    )
    panels = [  ]
    content_panels = Page.content_panels + [ 
        FieldPanel(“body”),
        FieldPanel(“date”),
        FieldPanel(“feed_image”),
        StreamFieldPanel(“content”)
    ]
    parent_page_types = ['blog.BlogIndex']
    Search_fields = Page.search_fields + [ 
        index.SearchField('body'),
        index.FilterField('date'),
    ]
    subpage_types = [  ]
    promote_panels = [  ]

    # This is classic django
    class Meta:
        verbose_name = “Home Page”
        verbose_name_plural = “Home Pages”
```


**Wagtail Attribute**
```python
panels = [  ]
# for admin site.
content_panels = Page.content_panels + [ 
    FieldPanel(“field_name”)
]
parent_page_types = ['blog.BlogIndex']
Search_fields = Page.search_fields + [  ]
subpage_types = [  ]
promote_panels = [  ]
```


**Wagtail Fields**
```python
body = RichTextField()
image = models.ForeignKey(
    'wagtailimages.Image',null=True, blank=True,
    on_delete=models.SET_NULL, related_name='+'
)
```

**Wagtail Method**
```python
def get_context(self, request):
def server(self, request):
def get_template(self, request):
def get_url(self, *args, **kwargs):
def get_full_url(self, *args, **kwargs):
def get_url_parts(self, *args, **kwargs):
```