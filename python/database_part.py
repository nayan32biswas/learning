import datetime
import mysql.connector as mysql
from pymongo import MongoClient
from ..common.my_utils import (
    convert_date_time,
    get_next_nth_month,
    diff_month,
    month_last_date
)


class Mysql:
    def __init__(self, *args, **kwargs):
        try:
            self.sql_db = mysql.connect(
                host="localhost",
                user="nayan32biswas",
                passwd="nayan32biswas",
                database="a2i_new"
            )
        except Exception as error:
            print(error)

    def insert(self, query):
        try:
            cursor = self.sql_db.cursor()
            cursor.execute(query)
            self.sql_db.commit()
        except Exception as error:
            print(error)
        return True

    def insert_many(self, query_list):
        try:
            cursor = self.sql_db.cursor()
            for query in query_list:
                cursor.execute(query)
            self.sql_db.commit()
        except Exception as error:
            print(error)
        return True

    def get_column_name(self, query):
        try:
            cursor = self.sql_db.cursor()
            cursor.execute(query)
            column_name = cursor.fetchall()
        except Exception as error:
            print(error)
        table_key = [x[0] for x in column_name]
        return table_key

    def get_data(self, query):
        try:
            cursor = self.sql_db.cursor()
            cursor.execute(query)
            records = cursor.fetchall()
        except Exception as error:
            print(error)
        return records

    def get_data_as_dict(self, query):
        try:
            cursor = self.sql_db.cursor(dictionary=True)
            cursor.execute(query)
            records = cursor.fetchall()
        except Exception as error:
            print(error)
        return records

    def update(self, query):
        try:
            cursor = self.sql_db.cursor()
            cursor.execute(query)
            self.sql_db.commit()
        except Exception as error:
            print(error)
        return True

    def update_many(self, querys):
        try:
            cursor = self.sql_db.cursor()
            for query in querys:
                cursor.execute(query)
            self.sql_db.commit()
        except Exception as error:
            print(error)
        return True


class Mongodb:
    def set_connection(self, db_name="a2i"):
        try:
            conn = MongoClient()
            mongo_db = conn[db_name]
        except Exception as error:
            print(error)
        return mongo_db, conn

    def insert_one(self, table_name, data):
        mongo_db, conn = self.set_connection()
        try:
            collection = mongo_db[table_name]
            collection.insert_one(data)
        except Exception as error:
            print(error)
        finally:
            conn.close()

    def insert_many(self, table_name, data_list):
        mongo_db, conn = self.set_connection()
        try:
            collection = mongo_db[table_name]
            for data in data_list:
                collection.insert_one(data)
        except Exception as error:
            print(error)
        finally:
            conn.close()

    # added new
    def exists(self, table_name, match_dict):
        mongo_db, conn = self.set_connection()
        try:
            collection = mongo_db[table_name]
            result = collection.find(match_dict).count()
        except Exception as error:
            print(error)
        finally:
            conn.close()
        return result

    # added new
    def get_last_element(self, table_name, match_dict):
        mongo_db, conn = self.set_connection()
        try:
            collection = mongo_db[table_name]
            result = collection.find(match_dict, {"data.summary": {"$slice": -1}})
        except Exception as error:
            print(error)
        finally:
            conn.close()
        return result

    # added new
    def push_till_nth_month(self, table_name, indicator_id, from_date, n):
        result = self.get_last_element(table_name, {"indicator_id": indicator_id})
        for each in result:
            from_date = each["data"]["summary"][0]["date"]
            from_date = get_next_nth_month(from_date, 1)

        from_date = convert_date_time(from_date)
        to_date = get_next_nth_month(datetime.datetime.today(), n)
        to_date = convert_date_time(to_date)
        num_of_month = diff_month(from_date, to_date)
        # print(from_date, to_date, num_of_month)
        date = from_date
        for i in range(num_of_month):
            date = month_last_date(date)
            date = convert_date_time(date)
            data = {"date": date, "data": 0, "target_data": 0, "bi_data": 0}
            # Mongodb.update_many(table_name, {}, {"$pull": {"data.summary": {"date": temp_date}}})
            self.update_many(table_name, {"indicator_id": indicator_id}, {"$push": {"data.summary": data}})
            date = get_next_nth_month(date, 1)

    def find(self, table_name, match_dict, projection=None):
        mongo_db, conn = self.set_connection()
        try:
            collection = mongo_db[table_name]
            if projection is not None:
                result = collection.find(match_dict, projection)
            else:
                result = collection.find(match_dict)
        except Exception as error:
            print(error)
        finally:
            conn.close()
        return result

    def find_one(self, table_name, match_dict, projection=None):
        result = []
        mongo_db, conn = self.set_connection()
        try:
            collection = mongo_db[table_name]
            if projection is not None:
                result = collection.find_one(match_dict, projection)
            else:
                result = collection.find_one(match_dict)
        except Exception as error:
            print(error)
        finally:
            conn.close()
        return result

    def update(self, table_name, match_dict, new_data):
        mongo_db, conn = self.set_connection()
        try:
            collection = mongo_db[table_name]
            result = collection.update(match_dict, new_data, True)
        except Exception as error:
            print(error)
        finally:
            conn.close()
        return result

    def update_many(self, table_name, match_dict, new_data):
        mongo_db, conn = self.set_connection()
        try:
            collection = mongo_db[table_name]
            result = collection.update_many(match_dict, new_data, True)
        except Exception as error:
            print(error)
        finally:
            conn.close()
        return result

    def insert_or_update(self, table_name, match_dict, new_data):
        mongo_db, conn = self.set_connection()
        try:
            collection = mongo_db[table_name]
            result = collection.update(match_dict, new_data, True)
        except Exception as error:
            print(error)
        finally:
            conn.close()

    def insert_or_update_many(self, table_name, data_list):
        mongo_db, conn = self.set_connection()
        try:
            collection = mongo_db[table_name]
            for match_dict, new_data in data_list:
                collection.update_one(match_dict, new_data, True)
        except Exception as error:
            print(error)
        finally:
            conn.close()
