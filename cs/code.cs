using System;
using System.IO;
using System.Collections.Generic;

namespace Start
{
    class Program
    {
        static void Main(string[] args){
            // checkType();
            // checkDataConversion();
            // checkObject();
            // checkNullable();
            // checkArray();
            // checkString();
            // checkRead();
            // checkStruct();
            // checkSort();
            // checkSTL();
            checkCustomLinkedList();
        }
        static void checkCustomLinkedList(){
            CustomLinkedList list = new CustomLinkedList();
            list.Add(1); list.Add(2); list.Add(3); list.Add(4); list.ShowList();
            list.ShowReavList();
        }
        class CustomLinkedList
        {
            internal class Node{
                internal int data;
                internal Node next, prev;
                public Node(int data){
                    this.data = data;
                    this.next = null;
                    this.prev = null;
                    
                }
            }
            Node head = null, tail = null;
            public void Add(int data){
                Node newNode = new Node(data);
                if(this.head == null){
                    this.head = newNode;
                    this.tail = this.head;
                }
                else{
                    newNode.prev = this.tail;
                    this.tail.next = newNode;
                    this.tail = this.tail.next;
                }
            }
            public void ShowList(){
                Node temp = this.head;
                while(temp != null){
                    System.Console.Write("{0}, ", temp.data);
                    temp = temp.next;
                }
                System.Console.WriteLine();
            }
            public void ShowReavList(){
                Node temp = this.tail;
                while(temp != null){
                    System.Console.Write("{0}, ", temp.data);
                    temp = temp.prev;
                }
                System.Console.WriteLine();
            }
        }
        static void checkSTL(){
            // LIST
            List<int> list = new List<int>();
            list.Add(10); list.Add(12); list.Add(7); list.Add(11); list.Add(29); list.Add(17);
            list.Sort(compare);
            list.RemoveAt(list.Count-1); // As same as POP
            for(int i=0; i<list.Count; i++) System.Console.Write("{0} ",list[i]); System.Console.WriteLine();

            // Dictionary
            Dictionary<int, int> dict = new Dictionary<int, int>();
            dict.Add(3, 4); dict.Add(1, 2);
            dict[4] = 3; // New assign
            dict[3] = 6; // update key
            dict.Remove(5); // remove operation without error
            foreach( KeyValuePair<int, int> kvp in dict ) Console.Write("({0}, {1}) ", kvp.Key, kvp.Value); System.Console.WriteLine();
            // SortedDictionary
            SortedDictionary<int, int> SDict = new SortedDictionary<int, int>();
            SDict.Add(2, 4); SDict.Add(1, 2);
            foreach( KeyValuePair<int, int> kvp in SDict ) Console.Write("({0}, {1}) ", kvp.Key, kvp.Value); System.Console.WriteLine();

            // HashSet
            HashSet<int> set = new HashSet<int>();
            set.Add(10); set.Add(4); set.Add(6); set.Add(4); set.Add(12); set.Add(1); set.Add(12);
            set.Remove(4);
            foreach (int item in set) System.Console.Write("{0} ", item);System.Console.WriteLine();
            // SortedSet
            SortedSet<int> SSet = new SortedSet<int>();
            SSet.Add(10); SSet.Add(4); SSet.Add(6); SSet.Add(4); SSet.Add(12); SSet.Add(1); SSet.Add(12);
            foreach (int item in SSet) System.Console.Write("{0} ", item);System.Console.WriteLine();

            // LinkedList
            LinkedList<int> lList = new LinkedList<int>();
            lList.AddFirst(1); lList.AddLast(2); lList.AddFirst(3); lList.AddLast(4); lList.AddFirst(5); lList.AddLast(6);
            lList.RemoveFirst(); lList.RemoveLast();
            System.Console.WriteLine("{0}   {1}", lList.First, lList.Last);
            foreach (int item in lList)System.Console.Write("{0} ",item); System.Console.WriteLine();

            // Queue
            Queue<int> que = new Queue<int>();
            que.Enqueue(2);que.Enqueue(1);que.Enqueue(4);que.Enqueue(3);
            System.Console.WriteLine(que.Peek()); // Get front
            System.Console.WriteLine(que.Dequeue()); // Pop front
            foreach (int item in que)System.Console.Write("{0} ", item); System.Console.WriteLine();

            // Stack
            Stack<int> stack = new Stack<int>();
            stack.Push(2);stack.Push(1);stack.Push(4);stack.Push(3);
            System.Console.WriteLine(stack.Peek()); // Get top
            System.Console.WriteLine(stack.Pop()); // Pop top
            foreach (int item in stack)System.Console.Write("{0} ", item); System.Console.WriteLine();
        }
        static void checkDataConversion(){
            float f = 120.123f;
            string str = "123123";
            int num = (int)f;
            num = int.Parse(str);
            System.Console.WriteLine(num);
        }
        static void checkSort(){
            int[] arr = new int[] {1, 9, -1, 7, 5, 9};
            Array.Reverse(arr);        
            Array.Sort(arr);
            Array.Sort<int>(arr, delegate(int a, int b) { return compare(a, b); });
            Array.Sort(arr, compare);

            Book[] Books = checkStruct();
            Array.Sort(Books, compareBook);
            printBooks(Books);
        }
        static bool isLessThanBook(Book A, Book B){
            if(A.title.CompareTo(B.title)<0)return true;
            if(A.title.CompareTo(B.title)==0){
                if(A.subject.CompareTo(B.subject)<0)return true;
                if(A.subject.CompareTo(B.subject)==0){
                    if(A.author.CompareTo(B.author)<0)return true;
                    if(A.author.CompareTo(B.author)==0){
                        return A.book_id<B.book_id;
                    }
                }
            }
            return false;
        }
        static int compareBook(Book A, Book B){return isLessThanBook(A, B) ? -1 : 1;}
        static bool isLessthan(int a, int b){return a<b;}
        static int compare(int a, int b){return isLessthan(a, b) ? -1 : 1;}
        static Book[] checkStruct(){
            Book one;
            one.title = "book__title";
            one.author = "book__author";
            one.subject = "book__subject";
            one.book_id = 1;
            // System.Console.WriteLine(one);

            Book[] Books = {
                new Book("title 1", "subject 1", "author 1", 7),
                new Book("title 2", "subject 2", "author 2", 2),
                new Book("title 1", "subject 1", "author 1", 6),
                new Book("title 1", "subject 1", "author 1", 1),
                new Book("title 3", "subject 3", "author 3", 3),
                new Book("title 1", "subject 1", "author 2", 4),
                new Book("title 1", "subject 2", "author 1", 5),
            };
            return Books;
        }
        static void printBooks(Book[] Books){
            foreach (Book item in Books)System.Console.WriteLine(item);
            System.Console.WriteLine();
        }
        static void print(int[] arr){
            foreach (int item in arr)System.Console.Write("{0} ", item);
            System.Console.WriteLine();
        }

        static void checkType(){
            var anyDataType = "AnyData"; // Any data type can be assign
            int myNum = 5, myNumMin = int.MinValue, myNumMax = int.MaxValue;               // 4B Integer (whole number)
            long myLongNum = 555, myLongNumMin = long.MinValue, myLongNumMax = long.MaxValue;        // 8B Log Integer (whole number)
            ulong myULongNum = 555, myULongNumMin = ulong.MinValue, myULongNumMax = ulong.MaxValue;      // 8B Unsigned Log Integer (whole number)
            float myFloatNum = 5.99f, myFloatNumMin = float.MinValue, myFloatNumMax = float.MaxValue;    // 4B Floating point number
            double myDoubleNum = 5.99D, myDoubleNumMin = double.MinValue, myDoubleNumMax = double.MaxValue;  // 8B Double point number
            decimal myDecimalNum = 5.99M, myDecimalNumMin = decimal.MinValue, myDecimalNumMax = decimal.MaxValue;// 16B Long Decimal point number
            bool myBool = true;          // 2B Boolean
            char myLetter = 'D';         // 1B Character
            string myText = "Hello";     // n*2B String
            // char* pointerChar;

            System.Console.WriteLine("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9}",anyDataType, myNum, myLongNum, myULongNum, myFloatNum, myDoubleNum, myDecimalNum, myBool, myLetter, myText);
            System.Console.WriteLine("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9}",anyDataType.ToString(), myNum.ToString(), myLongNum.ToString(), myULongNum.ToString(), myFloatNum.ToString(), myDoubleNum.ToString(), myDecimalNum.ToString(), myBool.ToString(), myLetter.ToString(), myText.ToString());

            System.Console.WriteLine("Num:          {0} {1} {2}", myNumMin, myNum, myNumMax);
            System.Console.WriteLine("LongNum:      {0} {1} {2}", myLongNumMin, myLongNum, myLongNumMax);
            System.Console.WriteLine("ULongNum:     {0} {1} {2}", myULongNumMin, myULongNum, myULongNumMax);
            System.Console.WriteLine("FloatNum:     {0} {1} {2}", myFloatNumMin, myFloatNum, myFloatNumMax);
            System.Console.WriteLine("DoubleNum:    {0} {1} {2}", myDoubleNumMin, myDoubleNum, myDoubleNumMax);
            System.Console.WriteLine("DecimalNum:   {0} {1} {2}", myDecimalNumMin, myDecimalNum, myDecimalNumMax);
        }
        static void checkObject(){
            Rectangle obj = new Rectangle(4, 3);
            int area = obj.area();
            System.Console.WriteLine(area);
        }
        static void checkNullable(){
            int? num = null;
            System.Console.WriteLine("Nullable {0}", num);
        }
        static void checkArray(){
            int[] nums;
            nums = new int[5];
            int[] nums1 = new int[5];
            int[] nums2 = {1, 2, 3, 4, 5};
            int[] nums3 = new int[5] {1, 2, 3, 4, 5};
            int[] nums4 = new int[] {1, 2, 3, 4, 5};
            System.Console.WriteLine("{0} {1} {2} {3} {4}", nums, nums1, nums2, nums3, nums4);
        }
        static void checkString(){
            string fname = "Rowan", lname = "Atkinson";
            string fullname = fname+lname;
            System.Console.WriteLine(fullname);

            char []letters= { 'H', 'e', 'l', 'l','o' };
            string [] sarray={ "Hello", "From", "Tutorials", "Point" };

            string greetings = new string(letters);
            string message = String.Join(" ", sarray);
            System.Console.WriteLine(greetings+"\t:\t"+message);

            DateTime waiting = new DateTime(2012, 10, 10, 17, 58, 1);
            string chat = String.Format("Message sent at {0:t} on {0:D}", waiting);
            System.Console.WriteLine(chat);

            string str = "demostring";
            for(int i=0; i<str.Length; i++) System.Console.Write(str[i]);
            System.Console.WriteLine();
        }
        static void checkRead(){
            string[] value = System.Console.ReadLine().Split();
            System.Console.WriteLine(value);

            string file = @"/home/work/prog/contest/cs/input.txt";
            // To read a text file line by line
            if (File.Exists(file)) { 
                string[] lines = File.ReadAllLines(file); 
                foreach(string ln in lines) 
                    Console.WriteLine(ln); 
            }
            // By using StreamReader
            if (File.Exists(file)) {
                // Reads file line by line
                StreamReader Textfile = new StreamReader(file);
                string line;
                while ((line = Textfile.ReadLine()) != null)
                    Console.WriteLine(line);
                Textfile.Close();
                // Console.ReadKey();
            }
        }
        static int binary_serarch_lower(int[] arr, int key)
        {
            int start = 0, end = arr.Length - 1, mid;
            while (end - start > 1)
            {
                mid = (start + end) / 2;
                if (arr[mid] < key)
                    start = mid;
                else
                    end = mid;
            }
            if (arr[end] <= key)
                return end;
            return start;
        }
        static int binary_serarch_upper(int[] arr, int key)
        {
            int start = 0, end = arr.Length - 1, mid;
            while (end - start > 1)
            {
                mid = (start + end) / 2;
                if (arr[mid] > key)
                    end = mid;
                else
                    start = mid;
            }
            if (arr[start] >= key)
                return start;
            return end;
        }
    }

    struct Book {
        public string title, author, subject;
        public int book_id;
        public Book(string title, string subject, string author,  int book_id){
            this.title = title;
            this.subject = subject;
            this.author = author;
            this.book_id = book_id;
        }
        public override string ToString(){
            return this.title+" "+this.subject+" "+this.author+" "+this.book_id.ToString();
        }
    };
    class Shape {
        protected int width, height;
        public Shape( int a = 0, int b = 0) {
            width = a;
            height = b;
        }
        public virtual int area() {
            Console.WriteLine("Parent class area :");
            return 0;
        }
    }
    class Rectangle: Shape {
        public Rectangle(int a=0,int b=0): base(a, b) {}
        public override int area () {
            return width * height; 
        }
        public void Display() {
            Console.WriteLine("height: {0}", height);
            Console.WriteLine("Width: {0}", width);
            Console.WriteLine("Area: {0}", area());
        }
        // Overload + operator to add two Box objects.
        // (operator + ), (operator == ), (operator != ), (operator < ), (operator > ), (operator <= ), (operator >= )
        public static Rectangle operator+ (Rectangle A, Rectangle B) {
            Rectangle rectangle = new Rectangle();
            rectangle.height = A.width + B.width;
            rectangle.height = A.height + B.height;
            return rectangle;
        }
    }
}