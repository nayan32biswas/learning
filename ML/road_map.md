# ML Road Map

## Initial Data handling

1. Import data set.
2. Takecare of missing data.
3. Handle Categorical data. Create seperate column for each category.
4. Splitting the Dataset into Training set and test set. Test set maximum 30%.
5. Feature Scalling.

### Dummy Variables

- When new create Dummy variable from Categorical variable we have to remove one column so total dummy variable `Dn-1`.

## Building A Model

5 methods of building models.

1. All-in
2. Backword Elimination
3. Forward Selection
4. Bidirectional Elimination
5. Score Comparison

### Backword Elimination

1. Select a significance level of stay in the model (e.g SL = 0.05)
2. Fit the full model with all possible predictors.
3. Consider the predictor with the highest P-value. If P > SL, go to the next step, otherwise take this model.
4. Remove the column where P > SL.
5. Fit Model without this variable. If all not good then go to step

### Forward Selection

1. Select a significance level to enter the model (e.g. SL = 0.05)
2. Fit all simple regression models y - xn select the one with the lowest P-value.
3. Keep the variable and fit all possible models with one extra predictor added to the one(s) you already have.
4. Consider the predictor with the lowest P-value. If P < SL, go to step 3, otherwise take this model.

### Bidirectional Elimination

1. Select a significance level of enter and to stay in the model (e.g. SLENTER = 0.05, SLSTAY = 0.05)
2. Perform the next step for Forward Selection (new variables mus have: P < SLENTER to enter)
3. Perform all steps of Backward Elimination (old variables must have P < SLSTAY to stay). Continue to step 2 if not satisfie all condition otherwise go to next step.
4. No new variables can enter and no old variables can exit. Then come final model.

### All Possible Models

1. Select a criterion of goodness of fit (e.g. Akaike criterion)
2. Construct all possible regression models (2^N)-1 total combinations
3. Select the one with the best criterion.

## Simple Linear Regression

`y = b0 + b1*X1` here `y` is a **Dependent variable**, `X1` is a **Independent variable**, `b1` is **Coefficient** and `b0` is **constent**.

## Multiple Linear Regression

`y = b0 + b1*X1 + b2*X2 + ... + bn*Xn` where `y` is a **Dependent variable**, all `X` is a **Independent variable**, all `b` is **Coefficient** and `b0` is **constent**.

### Assumptions of a Linear Regression

1. Linearity.
2. Homoscedasticity.
3. Multivariate normality.
4. Independence of errors.
5. Lack of multicollinearity.

## Polynomial Linear Regression

`y = b0 + b1*x1 + b2*x1^2 + ... + bn*x1^n`

## SVR

- `T = {X + Y}`
- `F(X) = Y`

1. Colect a training set `T = {X, Y}`
2. Choose a kernel and it's parameters as well as any regularization needed.
3. Form the correlation matrix, `K`
4. Train your machine, exactly or approximately, to get contraction coefficients `a = {ai}`
5. Use those coefficients, create your estimator `f(X, a, x*) = y*`

- `K(i,j) = exp(Sum-k theta|xki-xkj|^2) + e&i,j` Correlation Matrix

## Decision Tree
 Intuation

### CART

1. Classification Trees.
2. Regression Trees.

## Random Forest Intuation

- Create multiple decision tree
 and select average of all trees result to predict somthing that's Help to bo more accuret and stable prediction.

## Root Squere

R^2 greater is better.

- `R^2 = 1-(SS__res/SS__tot)`
- `y = b__0 + b__1*X__1 + b__2*X__2 + b__3*X__3 + ....`
- `SS__res -> Min`

## Logistic Regression

- `y = b__0 + b__1*x` --> `ln(p/(1-p)) = b__0 + b__1*x` Linear Regression --> Logistic Regression.

## K-NN

## SVM

## kernel

Most Popular kernal are.

- Gaussian RBF Kernal
- Sigmoid Kernal
- Polynomial Kernal

### Gaussian RBF Kernel

- `K((x)->, (l)->^i) = e^-((abs((x)->, (l)->^i)^2)/(2*sigma^2))`

## Naive Bayes

- `P(A|B) = ((P(B|A)*P(A))/P(B))`

Let's say you have 25(read) and 30(Blue) ball. New ball come into the place. Then what is the probability new ball become Read or Blue.

P(Read|new) V.S. P(Blue|new)

1. Create a random vertual circule aroud new ball
2. Some blue and some read ball cover by circle. Let's say inside circle Red=5 and Blue=8

N = 55
r = 25, b = 30
Nc = 13
rc = 5, bc = 8

### For P(Read|new)

- `P(Read) = (Number of Read Ball)/(Total number of ball)` = `r/N` = `25/55`
- `P(new) = (Total ball inside random circle)/(Total number of ball)` = `Nc/N` = `13/55`
- `P(new|Read) = (Number of Read Ball inside random circle)/(Total Read ball)` = `rc/r` = `5/25`

- `P(Read|new) = ((P(new|Read)*P(Read))/P(new))` = `((5/25)*(25/55))/(13/55)` = `0.384615385`

### For P(Blue|new)

- `P(Blue) = (Number of Blue Ball)/(Total number of ball)` = `b/N` = `30/55`
- `P(new) = (Total ball inside random circle)/(Total number of ball)` = `Nc/N` = `13/55`
- `P(new|Blue) = (Number of Blue Ball inside random circle)/(Total Blue ball)` = `bc/b` = `8/30`

- `P(Blue|new) = ((P(new|Blue)*P(Blue))/P(new))` = `((8/30)*(30/55))/(13/55)` = `0.615384615`

Here `P(Blue|new)>P(Read|new)` So **new ball become Blue**

## Decision Tree Classification

## CAP Analysis

X < 60% Rubbish model
60% < X < 70% Poor model
70% < X < 80% Good model
80% < X < 90% Very Good model
90% < X < 100% Too Good model and there a chance to become a overfit model.

## K-Means

1. Choose the number K of clusters.
2. Select random K points, the centroids (not necessarily from your dataset).
3. Assign each data point to the closest centroid --> That forms K clusters.
4. Compute and place the new centroid(center or middle point) of each cluster.
5. Reassign each data point to the new closest centroid. If any reassignment took place, go to STEP 4, otherwise go to FIN.

### K-Means++
