# Create Facebook BOT

# Setup Facebook APP

1. Create new app.
2. Select messenger app.
3. Select your page.
4. Select messages and messaging_postback as marked.
5. inside Webhooks
   - Set callback url `https://www.example.com/facebook/callback/` that is url on your site.
   - Set `VERIFY_TOKEN`
6. Test app functionality on your facebook page.
7. Try to approve your app from `App Review.`

## By Third Party APP (dialogflow)[https://dialogflow.com/]

1. Create App as like username.
2. Look at `Small Talk` and fillup all filed.
3. Create `Intents` and take action against given field.
4. Create `Entities` and take action against given filed. This is actual ML part.
5. Look at `Integrations` and makr Messegenger
    - Set `verify token` your custom token.
    - Set `Access Token` from facebook.
    - Copy `Callback` and set to facebook.

## Custom By flask.

- You can use any framework.

```py
from pymessenger import Bot
bot = Bot(FACEBOOK_ACCESS_TOKEN)

# ML part
def process_message(message):
    message = message.lower()
    return message

# Request part.
@app.route("/facebook/callback/", methods=["GET", "POST"])
def facebook_bot(*args, **kwargs):
    if request.method == "GET":
        if request.args.get("hub.verify_token") == VERIFY_TOKEN:
            return request.args.get("hub.challenge")
        else:
            return "Token is not valid."
    elif request.method == "POST":
        payload = request.json
        event = payload["entry"][0]["messaging"]
        for msg in event:
            text = msg["message"]["text"]
            sender_id = msg["sender"]["id"]
            reply = process_message(text)
            bot.send_text_message(sender_id, reply)
        return "MESSAGE RECEIVED."
    else:
        print(request.form)
        return "METHOD NOT VALID"
```