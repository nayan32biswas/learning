# Default Server Setup

1. sudo apt-get install htop
2. install git
3. install docker
4. mkdir /www && sudo chown -R $USER:$USER /www;
5. Set Alies
   ```sh
    alias nnp="cd /www/"
    alias nnd="cd /www/devspreneur/"
    alias nnv="cd /var/log/"
    alias nnng="cd /etc/nginx/conf.d/"
    ```
